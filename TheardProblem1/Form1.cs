﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TheardProblem1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Thread redThread;
        Thread blueThread;
        Thread greenThread;
        Thread yellowThread;
        Random newRandom;

        private void Form1_Load(object sender, EventArgs e)
        {
            newRandom = new Random();
        }

        public void startRedThread()
        {
            for (int i = 0; i < 100; i++)
            {
                this.CreateGraphics().DrawRectangle(new Pen(Brushes.Red, 5), new Rectangle(newRandom.Next(0, this.Width), newRandom.Next(0, this.Height), 5, 5));
                Thread.Sleep(50);
            }
            //MessageBox.Show("complite thearding");
        }

        public void startBlueThread()
        {
            for (int i = 0; i < 100; i++)
            {
                this.CreateGraphics().DrawRectangle(new Pen(Brushes.Blue, 5), new Rectangle(newRandom.Next(0, this.Width), newRandom.Next(0, this.Height), 5, 5));
                Thread.Sleep(50);
            }
            //MessageBox.Show("complite thearding");
        }

        public void startGreenThread()
        {
            for (int i = 0; i < 100; i++)
            {
                this.CreateGraphics().DrawRectangle(new Pen(Brushes.Green, 5), new Rectangle(newRandom.Next(0, this.Width), newRandom.Next(0, this.Height), 5, 5));
                Thread.Sleep(50);
            }
            //MessageBox.Show("complite thearding");
        }

        public void startYellowThread()
        {
            for (int i = 0; i < 100; i++)
            {
                this.CreateGraphics().DrawRectangle(new Pen(Brushes.Yellow, 5), new Rectangle(newRandom.Next(0, this.Width), newRandom.Next(0, this.Height), 5, 5));
                Thread.Sleep(50);
            }
            //MessageBox.Show("complite thearding");
        }

        private void red_Click(object sender, EventArgs e)
        {
            redThread = new Thread(startRedThread);
            blueThread = new Thread(startBlueThread);
            greenThread = new Thread(startGreenThread);
            yellowThread = new Thread(startYellowThread);

            redThread.Start();
            blueThread.Start();
            greenThread.Start();
            yellowThread.Start();
        }

        private void blue_Click(object sender, EventArgs e)
        {
            
        }
    }
}
